# Remarkable/module-template

This is a template module for the Remarkable.net modules system. It's purpose is to get you started with basic scaffolding for creating your own module.

## Setting up a new module

To get started, follow these instructions;

* [Download the template source code](https://gitlab.com/remarkablenet/frontend-modules/template/-/archive/master/template-master.zip) and extract it to a folder you'll be working from
* Start a command prompt in that folder
* Run `npm i`
* Create your module in `src` and the unit tests in `test`

## NPM commands

Command | Function
--- | ---
`npm run test ` | Run unit tests
`npm run test:w` | Run unit tests and watch for code changes, re-running tests live
`npm run cover` | Run unit tests and check code coverage
`npm run build` | Build the module in development (uncompressed) mode
`npm run build:w` | Build the module in development (uncompressed) mode and watch for code changes
`npm run build:p` | Build the module in production (compressed) mode

## Testing the module in your project

To test the module in your project during development, use `npm link` (https://docs.npmjs.com/cli/link.html)

For example;
```
cd ~/frontend-modules/template
npm link
cd ~/projects/your-project
npm link template
```

This will "trick" NPM to point to your local development folder when importing the module.

## Registering the module on NPM

Once you're ready to publish or update your module, make sure;

* the code is committed to git
* all unit tests pass
* code coverage is >75%

To publish or update the module on NPM;

* Update the version of your module with: `npm version [major|minor|patch]`
* Publish your module with: `npm publish`
* Push the version change to git: `git push`